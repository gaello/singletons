﻿using UnityEngine;

/// <summary>
/// This class shows how you can call singleton without having to worry about providing reference.
/// </summary>
public class UsageExample : MonoBehaviour
{
    // Key to store data.
    private const string SAVE_KEY = "Secret Info";
    // Timer used to print less logs.
    private float timer;

    /// <summary>
    /// Unity method called before first frame.
    /// </summary>
    private void Start()
    {
        GenerateNewValue();
    }

    /// <summary>
    /// Unity method called every frame.
    /// </summary>
    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            PrintSecretValue();
        }
    }

    /// <summary>
    /// Method used to generate new data and save it in data storage.
    /// Also sets timer with new value.
    /// </summary>
    private void GenerateNewValue()
    {
        DataStorage.Instance.SaveData(SAVE_KEY, Random.Range(1, 100));
        timer = Random.Range(0.5f, 3f);
    }


    /// <summary>
    /// Method used to print saved value in data storage.
    /// After print, method calls GenerateNewValue().
    /// </summary>
    private void PrintSecretValue()
    {
        var value = DataStorage.Instance.GetData<int>(SAVE_KEY);

        Debug.LogFormat("Secret value is: {0}", value);

        GenerateNewValue();
    }
}
