# Singleton Design Pattern for Unity

I assume that you are looking for some information about how to implement Singleton design pattern in Unity.

This repository contains example how you can do it! In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/04/04/singleton-in-unity-love-or-hate/

Enjoy!

---

# How to use it?

This repository contains an example of how you can implement singleton in a few ways and how you can call it later.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/singletons/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned about implementing Singleton design pattern in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com

